package org.provatesting.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used to identify a method as a validation method with each row representing a distinct result.
 * Each row in the result is matched as a distinct row against the list of rows of the
 * expected table
 */
@ProvaMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface SingleRowValidation {

  /**
   * Specifies which method is used to set up common values for this method.
   * @return the name of the method to use
   */
  String init() default "";
}
