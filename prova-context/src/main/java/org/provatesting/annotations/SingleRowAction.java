package org.provatesting.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used to identify a method as an action method with each row representing a distinct modification.
 * Generally, this type of action is used to create/update/delete data
 */
@ProvaMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface SingleRowAction {

  /**
   * Specifies which method is used to set up common values for this method.
   * @return the name of the method to use
   */
  String init() default "";
}
