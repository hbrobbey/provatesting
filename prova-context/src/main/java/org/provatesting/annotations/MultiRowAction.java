package org.provatesting.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used to identify a method as an action method with multiple rows representing a batch.
 * Generally, this type of method is used to update multiple rows as part of one transaction
 */
@ProvaMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface MultiRowAction {

  /**
   * Specifies which method is used to set up common values for this method.
   * @return the name of the method to use
   */
  String init() default "";
}
