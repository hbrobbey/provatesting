package org.provatesting.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Used to identify a method as a validation method with multiple rows representing a result.
 * All rows in the result are matched as a matrix against the matrix of rows/columns of the
 * expected table
 */
@ProvaMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface MultiRowValidation {

  /**
   * Specifies which method is used to set up common values for this method.
   * @return the name of the method to use
   */
  String init() default "";
}
