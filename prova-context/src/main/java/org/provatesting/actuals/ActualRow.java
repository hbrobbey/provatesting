package org.provatesting.actuals;

import java.util.HashSet;
import java.util.Set;

public class ActualRow {

  private Set<ActualColumn> actualColumns;

  public ActualRow() {
    this.actualColumns = new HashSet<>();
  }

  public void addActualColumn(ActualColumn actualColumn) {
    actualColumns.add(actualColumn);
  }

  public Set<ActualColumn> getActualColumns() {
    return actualColumns;
  }
}
