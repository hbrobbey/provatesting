package org.provatesting.expectations;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

public class ExpectedRow implements Comparable<ExpectedRow> {

  private int rowNumber;
  private SortedSet<ExpectedColumn> expectedColumns;
  private Map<String, ExpectedColumn> expectedColumnsByKey;

  /**
   * Represents a row of the actual table that was specified in the test.
   * @param rowNumber - the position of the row in the table
   */
  public ExpectedRow(int rowNumber) {
    this.expectedColumns = new TreeSet<>();
    this.expectedColumnsByKey = new HashMap<>();
    this.rowNumber = rowNumber;
  }

  public int getRowNumber() {
    return rowNumber;
  }

  public SortedSet<ExpectedColumn> getExpectedColumns() {
    return expectedColumns;
  }

  public void addExpectedColumn(ExpectedColumn column) {
    expectedColumns.add(column);
    expectedColumnsByKey.put(column.getKey(), column);
  }

  /**
   * Retrieves the expected cell for this row for a given name.
   * @param key - the name of the column
   * @return the expected cell
   * @throws KeyNotFound exception if the key is not present
   */
  //TODO: maybe add a getColumnValue instead that is the right type?
  public ExpectedColumn getColumn(String key) {
    if (expectedColumnsByKey.containsKey(key)) {
      return expectedColumnsByKey.get(key);
    } else {
      throw new KeyNotFound(key, rowNumber);
    }
  }

  @Override
  public int compareTo(ExpectedRow o) {
    return Integer.compare(rowNumber, o.rowNumber);
  }

  /**
   * Compares an instance of this type with a given object.
   * @param object - the object to compare to
   * @return true, only if the two objects are of the same type and all properties are equal
   */
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || getClass() != object.getClass()) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    ExpectedRow that = (ExpectedRow) object;
    return rowNumber == that.rowNumber
        && Objects.equals(expectedColumns, that.expectedColumns)
        && Objects.equals(expectedColumnsByKey, that.expectedColumnsByKey);
  }

  public int hashCode() {
    return Objects.hash(super.hashCode(), rowNumber, expectedColumns, expectedColumnsByKey);
  }

  public static class KeyNotFound extends RuntimeException {
    public KeyNotFound(String key, int rowNumber) {
      super("Key " + key + " not found in row " + rowNumber);
    }
  }
}
