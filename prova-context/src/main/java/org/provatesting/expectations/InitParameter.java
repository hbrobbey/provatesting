package org.provatesting.expectations;

import java.util.Objects;

public class InitParameter implements Comparable<InitParameter> {

  private int parameterIndex;
  private String name;
  private String value;

  /**
   * Represents a value used to initialze a resource's action.
   * @param parameterIndex - the position of the init param in relation to its siblings
   * @param paramName - the human readable name
   * @param paramValue - the actual value
   */
  public InitParameter(int parameterIndex, String paramName, String paramValue) {
    this.parameterIndex = parameterIndex;
    this.name = paramName;
    this.value = paramValue;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }

  @Override
  public int compareTo(InitParameter o) {
    return Integer.compare(parameterIndex, o.parameterIndex);
  }

  /**
   * Compares an instance of this type with a given object.
   * @param object - the object to compare to
   * @return true, only if the two objects are of the same type and all properties are equal
   */
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || getClass() != object.getClass()) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    InitParameter that = (InitParameter) object;
    return parameterIndex == that.parameterIndex
        && Objects.equals(name, that.name)
        && Objects.equals(value, that.value);
  }

  public int hashCode() {
    return Objects.hash(super.hashCode(), parameterIndex, name, value);
  }
}
