package org.provatesting.expectations;

public enum DataType {
  Text,
  Number,
  LocalDate
}
