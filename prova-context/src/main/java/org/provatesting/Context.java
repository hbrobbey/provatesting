package org.provatesting;

public interface Context {

  Class get(String resourceName);
}
