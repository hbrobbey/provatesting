package org.provatesting.expectations;

import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;

public class ExpectedRowTest {

  @Test
  public void getColumn() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "key1", "", "", "value1"));

    ExpectedColumn col = expectedRow.getColumn("key1");
    assertNotNull(col);

    assertEquals("value1", col.getValue());
  }

  @Test(expectedExceptions = ExpectedRow.KeyNotFound.class)
  public void getColumn_throwsRuntimeExceptionIfKeyIsNotFound() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.getColumn("key1");
  }
}
