# PROVA

A JAVA-based integration test framework that lets users (developers/QAs/BAs) write tests in a format that is familiar to them (e.g. XLS).

For sample projects and use cases, please go to http://provatesting.org.

### Building the project
To build the project all you need to do is: `mvn install`:
* Build with Maven 3.3.9
* Run Maven with JDK 1.8

More details can be found in the `pom.xml`.

### Contributing
To contribute, please follow the guidelines in [Contributing](CONTRIBUTING.md). Also, please read and follow our [Code of Conduct](CODE_OF_CONDUCT.md).
