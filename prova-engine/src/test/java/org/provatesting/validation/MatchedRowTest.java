/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.validation;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MatchedRowTest {

  @Test
  public void fullMatch() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "key1", "Text", "", "value1"));
    expectedRow.addExpectedColumn(new ExpectedColumn(2, "another_key1", "Text", "", "value2"));

    ActualRow actualRow = new ActualRow();
    actualRow.addActualColumn(new ActualColumn("another_key1", "value2"));
    actualRow.addActualColumn(new ActualColumn("key1", "value1"));

    MatchedRow matchedRow = new MatchedRow(expectedRow, actualRow);

    assertEquals(matchedRow.getMatchRate(), 2.11);
    assertEquals(matchedRow.getMatchPercent(), 1d);

    assertEquals(matchedRow.getMatchedColumns().size(), 2);

    assertEquals(matchedRow.getMatchedColumns().get(0).getKey(), "key1");
    assertEquals(matchedRow.getMatchedColumns().get(1).getKey(), "another_key1");

    assertEquals(matchedRow.getUnmatchedExpectedColumns().size(), 0);
    assertEquals(matchedRow.getUnmatchedActualColumns().size(), 0);
  }

  @Test
  public void valuesMismatched() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "key1", "Text", "", "valueY"));

    ActualRow actualRow = new ActualRow();
    actualRow.addActualColumn(new ActualColumn("key1", "value1"));

    MatchedRow matchedRow = new MatchedRow(expectedRow, actualRow);

    assertEquals(matchedRow.getMatchRate(), 0d);
    assertEquals(matchedRow.getMatchPercent(), 0d);

    assertEquals(matchedRow.getMatchedColumns().size(), 1);
    assertEquals(matchedRow.getUnmatchedExpectedColumns().size(), 0);
    assertEquals(matchedRow.getUnmatchedActualColumns().size(), 0);
  }

  @Test
  public void oneColumnMissingInActuals() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "key1", "Text", "", "value1"));

    ActualRow actualRow = new ActualRow();

    MatchedRow matchedRow = new MatchedRow(expectedRow, actualRow);

    assertEquals(matchedRow.getMatchRate(), 0d);
    assertEquals(matchedRow.getMatchPercent(), 0d);

    assertEquals(matchedRow.getMatchedColumns().size(), 0);
    assertEquals(matchedRow.getUnmatchedExpectedColumns().size(), 1);
    assertEquals(matchedRow.getUnmatchedActualColumns().size(), 0);
  }

  @Test
  public void oneColumnMissingInExpected() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "key1", "Text", "", "value1"));

    ActualRow actualRow = new ActualRow();
    actualRow.addActualColumn(new ActualColumn("key1", "value1"));
    actualRow.addActualColumn(new ActualColumn("key2", "XYZ"));

    MatchedRow matchedRow = new MatchedRow(expectedRow, actualRow);

    assertEquals(matchedRow.getMatchRate(), 1.1);
    assertEquals(matchedRow.getMatchPercent(), 1d);

    assertEquals(matchedRow.getMatchedColumns().size(), 1);
    assertEquals(matchedRow.getMatchedColumns().get(0).getKey(), "key1");

    assertEquals(matchedRow.getUnmatchedExpectedColumns().size(), 0);

    assertEquals(matchedRow.getUnmatchedActualColumns().size(), 1);
    assertEquals(matchedRow.getUnmatchedActualColumns().get(0).getKey(), "key2");
  }

  @Test
  public void matchRateHigherIfMismatchIsInAColumnWithAHigherIndex() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "key1", "Text", "", "a"));
    expectedRow.addExpectedColumn(new ExpectedColumn(2, "key2", "Text", "", "a"));
    expectedRow.addExpectedColumn(new ExpectedColumn(3, "key3", "Text", "", "a"));

    ActualRow actualRow = new ActualRow();
    actualRow.addActualColumn(new ActualColumn("key1", "a"));
    actualRow.addActualColumn(new ActualColumn("key2", "a"));
    actualRow.addActualColumn(new ActualColumn("key3", "_"));

    MatchedRow matchedRow = new MatchedRow(expectedRow, actualRow);
    assertEquals(matchedRow.getMatchRate(), 2.110);
    assertEquals(matchedRow.getMatchPercent(), 2.11 / 3.111);

    actualRow = new ActualRow();
    actualRow.addActualColumn(new ActualColumn("key1", "a"));
    actualRow.addActualColumn(new ActualColumn("key2", "_"));
    actualRow.addActualColumn(new ActualColumn("key3", "a"));

    matchedRow = new MatchedRow(expectedRow, actualRow);
    assertEquals(matchedRow.getMatchRate(), 2.101);
    assertEquals(matchedRow.getMatchPercent(), 2.101 / 3.111);
  }

  @Test
  public void matchIgnoresMagicInputStringInTheColumnName() {
    ExpectedRow expectedRow = new ExpectedRow(1);
    expectedRow.addExpectedColumn(new ExpectedColumn(1, "|input|key1", "Text", "", "a"));
    expectedRow.addExpectedColumn(new ExpectedColumn(2, "key2", "Text", "", "a"));

    ActualRow actualRow = new ActualRow();
    actualRow.addActualColumn(new ActualColumn("key2", "a"));

    MatchedRow matchedRow = new MatchedRow(expectedRow, actualRow);
    assertEquals(matchedRow.getMatchRate(), 2.11);
    assertEquals(matchedRow.getMatchPercent(), 1d);
  }
}
