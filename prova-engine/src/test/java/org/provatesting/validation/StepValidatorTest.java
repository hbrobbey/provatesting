/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.validation;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.scenario.Step;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.stream.Collectors;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class StepValidatorTest {

  @Test
  public void validate_validStep_AppropriatelyMatchedRows() {
    Step step = new Step(1, "resource", "action");
    step.addExpectedRow(buildExpectedRow(2, "a"));
    step.addActualRow(buildActualRow("a"));

    assertTrue(step.isValid());
    assertEquals(step.getMatchedRows().size(), 1);
    assertEquals(step.getUnmatchedExpectedRows().size(), 0);
    assertEquals(step.getUnmatchedActualRows().size(), 0);

    MatchedRow matchedRow = step.getMatchedRows().iterator().next();
    assertEquals(matchedRow.getMatchRate(), 1.1, 0.01);
  }

  @Test
  public void validate_invalidStep_AppropriatelyMatchedRows() {
    Step step = new Step(1, "resource", "action");
    step.addExpectedRow(buildExpectedRow(2, "a", "a"));
    step.addActualRow(buildActualRow("a", "b"));

    assertFalse(step.isValid());
    assertEquals(step.getMatchedRows().size(), 1);
    assertEquals(step.getUnmatchedExpectedRows().size(), 0);
    assertEquals(step.getUnmatchedActualRows().size(), 0);

    MatchedRow matchedRow = step.getMatchedRows().iterator().next();
    assertEquals(matchedRow.getMatchRate(), 1.1);
  }

  @Test(enabled = false)
  public void validate_invalidStep_SurplusRow() {
    Step step = new Step(1, "resource", "action");

    step.addActualRow(buildActualRow("a"));

    assertFalse(step.isValid());
    assertEquals(step.getMatchedRows().size(), 0);
    assertEquals(step.getUnmatchedExpectedRows().size(), 0);
    assertEquals(step.getUnmatchedActualRows().size(), 1);
  }

  @Test
  public void validate_invalidStep_MissingRow() {
    Step step = new Step(1, "resource", "action");
    step.addExpectedRow(buildExpectedRow(2, "a"));

    assertFalse(step.isValid());
    assertEquals(step.getMatchedRows().size(), 0);
    assertEquals(step.getUnmatchedExpectedRows().size(), 1);
    assertEquals(step.getUnmatchedActualRows().size(), 0);
  }

  @Test
  public void validate_invalidStep_SameNumberOfRowsButNotMatched_OneSurplusAndOneMissing() {
    Step step = new Step(1, "resource", "action");
    step.addExpectedRow(buildExpectedRow(2, "a"));

    step.addActualRow(buildActualRow("b"));

    assertFalse(step.isValid());
    assertEquals(step.getMatchedRows().size(), 0);
    assertEquals(step.getUnmatchedExpectedRows().size(), 1);
    assertEquals(step.getUnmatchedActualRows().size(), 1);
  }

  @Test
  public void validate_validStep_TwoRowsButKeysMightBeUnclear() {
    Step step = new Step(1, "resource", "action");
    step.addExpectedRow(buildExpectedRow(2, "a", "a", "_"));
    step.addExpectedRow(buildExpectedRow(3, "a", "a", "a"));
    step.addExpectedRow(buildExpectedRow(4, "a", "_", "a"));

    step.addActualRow(buildActualRow("a", "_", "a"));
    step.addActualRow(buildActualRow("a", "a", "a"));
    step.addActualRow(buildActualRow("a", "_", "_"));

    assertFalse(step.isValid());
    assertEquals(step.getMatchedRows().size(), 3);
    assertEquals(step.getUnmatchedExpectedRows().size(), 0);
    assertEquals(step.getUnmatchedActualRows().size(), 0);

    MatchedRow r2 = null, r3 = null, r4 = null;
    for (MatchedRow mr : step.getMatchedRows()) {
      if (mr.getExpectedRow().getRowNumber() == 2) {
        r2 = mr;
      } else if (mr.getExpectedRow().getRowNumber() == 3) {
        r3 = mr;
      } else {
        r4 = mr;
      }
    }

    validateColumns(new String[]{"a", "a", "_"}, new String[]{"a", "_", "_"}, r2);
    validateColumns(new String[]{"a", "a", "a"}, new String[]{"a", "a", "a"}, r3);
    validateColumns(new String[]{"a", "_", "a"}, new String[]{"a", "_", "a"}, r4);
  }

  @Test
  public void validate_validStep_noExpectedAndNoActualRows() {
    Step step = new Step(1, "resource", "action");

    assertTrue(step.isValid());
    assertEquals(step.getMatchedRows().size(), 0);
    assertEquals(step.getUnmatchedExpectedRows().size(), 0);
    assertEquals(step.getUnmatchedActualRows().size(), 0);
  }

  private void validateColumns(String[] expected, String[] actual, MatchedRow mr) {
    assertNotNull(mr);

    Map<String, String> expectedCols = mr.getExpectedRow().getExpectedColumns().stream()
        .collect(Collectors.toMap(ExpectedColumn::getKey, ExpectedColumn::getValue));

    for (int i = 0; i < expected.length; i++) {
      String key = "key" + (i + 1);
      assertEquals(expectedCols.get(key), expected[i], key);
    }

    Map<String, String> actualCols = mr.getActualRow().getActualColumns().stream()
        .collect(Collectors.toMap(ActualColumn::getKey, ActualColumn::getValue));

    for (int i = 0; i < actual.length; i++) {
      String key = "key" + (i + 1);
      assertEquals(actualCols.get(key), actual[i], key);
    }
  }

  @Test
  public void validate_validStep_exceptionThrown() {
    Step step = new Step(1, "resource", "action");
    step.addExpectedRow(buildExpectedRow(2, "a"));
    step.setExceptionMessage("My Exception Message");

    assertFalse(step.isValid());
    assertTrue(step.isException());
  }

  private ExpectedRow buildExpectedRow(int rowNumber, String... columnValues) {
    ExpectedRow er = new ExpectedRow(rowNumber);
    int columnNumber = 1;
    for (String expectedValue : columnValues) {
      er.addExpectedColumn(new ExpectedColumn(columnNumber, "key" + columnNumber, "Text", "", expectedValue));
      columnNumber++;
    }
    return er;
  }

  private ActualRow buildActualRow(String... columnValues) {
    ActualRow ar = new ActualRow();
    int columnNumber = 1;
    for (String actualValue : columnValues) {
      ar.addActualColumn(new ActualColumn("key" + columnNumber, actualValue));
      columnNumber++;
    }
    return ar;
  }

}
