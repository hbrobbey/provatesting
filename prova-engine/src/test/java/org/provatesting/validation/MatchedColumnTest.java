/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.validation;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.expectations.ExpectedColumn;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class MatchedColumnTest {

  @Test
  public void getKey() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "", "", "value1");
    ActualColumn actualColumn = new ActualColumn("key1", "value1");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertEquals(matchedColumn.getKey(), "key1");
  }

  @Test
  public void valueIsMatch_NoTypeUsesStringComparison_StringsAreEqual() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "", "", "value1");
    ActualColumn actualColumn = new ActualColumn("key1", "value1");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NoTypeUsesStringComparison_StringsAreNotEqual() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "", "", "value1");
    ActualColumn actualColumn = new ActualColumn("key1", "value2");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_StringsAreEqual() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Text", "", "value1");
    ActualColumn actualColumn = new ActualColumn("key1", "value1");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_StringsAreNotEqual() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Text", "", "value1");
    ActualColumn actualColumn = new ActualColumn("key1", "value2");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NumbersAreEqual() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.01", "123");
    ActualColumn actualColumn = new ActualColumn("key1", "123");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NumbersAreNotEqual_CompletelyDifferentValues() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.01", "123");
    ActualColumn actualColumn = new ActualColumn("key1", "456");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NumbersAreNotEqual_RoundingWithNonDefaultPrecision() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.001", "123.3334");
    ActualColumn actualColumn = new ActualColumn("key1", "123.3335");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NumbersAreEqual_Rounding_WithNonDefaultPrecision() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.001", "123.3334");
    ActualColumn actualColumn = new ActualColumn("key1", "123.3333");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NumbersAreNotEqual_RoundingWithDefaultPrecision() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.01", "123.334");
    ActualColumn actualColumn = new ActualColumn("key1", "123.335");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_NumbersAreEqual_Rounding_WithDefaultPrecision() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.01", "123.334");
    ActualColumn actualColumn = new ActualColumn("key1", "123.333");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedAndActualAreNull_meaningNoExpectedAndNoActualRows() {
    MatchedColumn matchedColumn = new MatchedColumn(null, null);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedNullActualNoNull() {
    ExpectedColumn expectedColumn = null;
    ActualColumn actualColumn = new ActualColumn("key1", "123.333");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedNotNullActualNull() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.01", "123.334");
    ActualColumn actualColumn = null;

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedNotNullActualNotNullButActualValueIsNull() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Number", "0.01", "123.334");
    ActualColumn actualColumn = new ActualColumn("key1", null);

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedValueIsAny() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Text", "", "@{ANY}@");
    ActualColumn actualColumn = new ActualColumn("key1", "AAA");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", "123");
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", "");
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", null);
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = null;
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedValueIsFlaggedAsNull() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Text", "", "@{NULL}@");
    ActualColumn actualColumn = new ActualColumn("key1", "AAA");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", "123");
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", "");
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", null);
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = null;
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());
  }

  @Test
  public void valueIsMatch_ExpectedValueIsFlaggedAsNotNull() {
    ExpectedColumn expectedColumn = new ExpectedColumn(1, "key1", "Text", "", "@{NOT_NULL}@");
    ActualColumn actualColumn = new ActualColumn("key1", "AAA");

    MatchedColumn matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", "123");
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertTrue(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", "");
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());

    actualColumn = new ActualColumn("key1", null);
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());

    actualColumn = null;
    matchedColumn = new MatchedColumn(expectedColumn, actualColumn);
    assertFalse(matchedColumn.valueIsMatch());
  }

  @Test(enabled = false)
  public void valueIsMatch_dates() {
    fail("Do we need this?!");
  }
}
