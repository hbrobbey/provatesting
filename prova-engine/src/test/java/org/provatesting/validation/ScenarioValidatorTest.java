/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.validation;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.scenario.Scenario;
import org.provatesting.scenario.Step;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class ScenarioValidatorTest {

  @Test(groups = "unit")
  public void test() {
    Scenario scenario = buildScenario();

    assertTrue(scenario.isValid());
  }

  private Scenario buildScenario() {
    Scenario scenario = new Scenario("Test Folder", "Test Scenario");

    Step step = new Step(1, "resource", "action1");

    step.addExpectedRow(buildExpectedRow());
    step.addActualRow(buildActualRow());
    scenario.addStep(step);

    return scenario;
  }

  private ExpectedRow buildExpectedRow() {
    ExpectedRow expectedRow = new ExpectedRow(2);

    expectedRow.addExpectedColumn(new ExpectedColumn(1, "R1_A1_V1", "Text", "", "R1_A1_V1"));
    expectedRow.addExpectedColumn(new ExpectedColumn(2, "R1_A1_V2", "Number", "0.01", "1.234"));

    return expectedRow;
  }

  private ActualRow buildActualRow() {
    ActualRow actualRow = new ActualRow();

    actualRow.addActualColumn(new ActualColumn("R1_A1_V1", "R1_A1_V1"));
    actualRow.addActualColumn(new ActualColumn("R1_A1_V2", "1.23"));

    return actualRow;
  }
}
