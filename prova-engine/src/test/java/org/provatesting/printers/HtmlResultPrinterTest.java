/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.printers;

import org.mockito.Mockito;
import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.expectations.InitParameter;
import org.provatesting.scenario.Scenario;
import org.provatesting.scenario.Step;
import org.provatesting.validation.MatchedRow;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class HtmlResultPrinterTest {

  @Test
  public void test() throws IOException {
    Scenario scenario = getScenario();

    Writer writer = Mockito.mock(Writer.class);
    ResultPrinter resultPrinter = new HtmlResultPrinter(writer);
    resultPrinter.print(scenario);

    Mockito.verify(writer).write(getExpectedHtml());
  }

  private Scenario getScenario() {
    Scenario scenario = new Scenario("TestFolder", "SampleTest.xlsx");

    scenario.addStep(new Step(1, "TestResource", "ActionMethod") {
      @Override
      public boolean isValid() {
        return true;
      }

      @Override
      public List<String> getKeys() {
        return Arrays.asList("Key_One", "KeyTwo", "Key_Three", "Key_Foo");
      }

      @Override
      public SortedSet<InitParameter> getInitParams() {
        SortedSet<InitParameter> params = new TreeSet<>();
        params.add(new InitParameter(1, "InitParamName", "InitParamValue"));
        return params;
      }

      @Override
      public Collection<MatchedRow> getMatchedRows() {
        ExpectedRow step1ExpectedRow = new ExpectedRow(4);
        step1ExpectedRow.addExpectedColumn(new ExpectedColumn(2, "Key_One", "Text", "", "Value_One"));
        step1ExpectedRow.addExpectedColumn(new ExpectedColumn(3, "KeyTwo", "Text", "", "ValueTwo"));
        step1ExpectedRow.addExpectedColumn(new ExpectedColumn(4, "Key_Three", "Text", "", "Value A"));
        step1ExpectedRow.addExpectedColumn(new ExpectedColumn(5, "Key_Foo", "Text", "", "Baz"));

        ActualRow step1ActualRow = new ActualRow();
        step1ActualRow.addActualColumn(new ActualColumn("Key_One", "Value_One"));
        step1ActualRow.addActualColumn(new ActualColumn("KeyTwo", "ValueTwo"));
        step1ActualRow.addActualColumn(new ActualColumn("Key_Three", "Value A"));
        step1ActualRow.addActualColumn(new ActualColumn("Key_Foo", "Baz"));

        return Arrays.asList(new MatchedRow(step1ExpectedRow, step1ActualRow));
      }

      @Override
      public SortedSet<ExpectedRow> getUnmatchedExpectedRows() {
        return new TreeSet<>();
      }

      @Override
      public Set<ActualRow> getUnmatchedActualRows() {
        return new HashSet<>();
      }
    });

    scenario.addStep(new Step(9, "TestResource", "ValidationMethod") {
      @Override
      public boolean isValid() {
        return false;
      }

      @Override
      public List<String> getKeys() {
        return Arrays.asList("Key_One", "KeyTwo", "Key_Three", "Key_Foo");
      }

      @Override
      public SortedSet<InitParameter> getInitParams() {
        SortedSet<InitParameter> params = new TreeSet<>();
        params.add(new InitParameter(1, "InitParamName", "InitParamValue"));
        return params;
      }

      @Override
      public Collection<MatchedRow> getMatchedRows() {
        ExpectedRow step2ExpectedRow1 = new ExpectedRow(4);
        step2ExpectedRow1.addExpectedColumn(new ExpectedColumn(2, "Key_One", "Text", "", "Value_One"));
        step2ExpectedRow1.addExpectedColumn(new ExpectedColumn(3, "KeyTwo", "Text", "", "ValueTwo"));
        step2ExpectedRow1.addExpectedColumn(new ExpectedColumn(4, "Key_Three", "Text", "", "Value A"));
        step2ExpectedRow1.addExpectedColumn(new ExpectedColumn(5, "Key_Foo", "Text", "", "Baz"));

        ActualRow step2ActualRow1 = new ActualRow();
        step2ActualRow1.addActualColumn(new ActualColumn("Key_One", "Value_One"));
        step2ActualRow1.addActualColumn(new ActualColumn("KeyTwo", "ValueTwo"));
        step2ActualRow1.addActualColumn(new ActualColumn("Key_Three", "Value B"));
        step2ActualRow1.addActualColumn(new ActualColumn("Key_Foo", "Baz"));

        return Arrays.asList(new MatchedRow(step2ExpectedRow1, step2ActualRow1));
      }

      @Override
      public SortedSet<ExpectedRow> getUnmatchedExpectedRows() {
        ExpectedRow step2ExpectedRow2 = new ExpectedRow(4);
        step2ExpectedRow2.addExpectedColumn(new ExpectedColumn(2, "Key_One", "Text", "", "bla"));
        step2ExpectedRow2.addExpectedColumn(new ExpectedColumn(3, "KeyTwo", "Text", "", "Blub"));
        step2ExpectedRow2.addExpectedColumn(new ExpectedColumn(4, "Key_Three", "Text", "", "Random Value"));
        step2ExpectedRow2.addExpectedColumn(new ExpectedColumn(5, "Key_Foo", "Text", "", "Baz"));

        return new TreeSet<>(Arrays.asList(step2ExpectedRow2));
      }

      @Override
      public Set<ActualRow> getUnmatchedActualRows() {
        ActualRow step2ActualRow3 = new ActualRow();
        step2ActualRow3.addActualColumn(new ActualColumn("Key_One", "BLA_"));
        step2ActualRow3.addActualColumn(new ActualColumn("KeyTwo", "blb"));
        step2ActualRow3.addActualColumn(new ActualColumn("Key_Three", "Random_Value"));
        step2ActualRow3.addActualColumn(new ActualColumn("Key_Foo", "bat"));

        return new HashSet<>(Arrays.asList(step2ActualRow3));
      }
    });

    Step step3 = new Step(18, "TestResource", "Method_CausingException");
    step3.setExceptionMessage("java.lang.RuntimeException: An error occured in the step\n" +
        "\tat org.provatesting.runner.ProvaRunnerTest$SampleContext$1.method_CausingException(ProvaRunnerTest.java:91)\n" +
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n" +
        "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n" +
        "\tat java.lang.reflect.Method.invoke(Method.java:497)\n" +
        "\tat org.provatesting.runner.ProvaRunner.processMultiRowAction(ProvaRunner.java:123)\n" +
        "\tat org.provatesting.runner.ProvaRunner.processBasedOnAnnotation(ProvaRunner.java:102)\n" +
        "\tat org.provatesting.runner.ProvaRunner.run(ProvaRunner.java:83)\n" +
        "\tat org.provatesting.runner.ProvaRunner.run(ProvaRunner.java:63)\n" +
        "\tat java.util.ArrayList$ArrayListSpliterator.forEachRemaining(ArrayList.java:1374)\n" +
        "\tat java.util.stream.ReferencePipeline$Head.forEach(ReferencePipeline.java:580)\n" +
        "\tat org.provatesting.runner.ProvaRunner.run(ProvaRunner.java:38)\n" +
        "\tat org.provatesting.runner.ProvaRunnerTest.testEndToEnd(ProvaRunnerTest.java:55)\n" +
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n" +
        "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n" +
        "\tat java.lang.reflect.Method.invoke(Method.java:497)\n" +
        "\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:86)\n" +
        "\tat org.testng.internal.Invoker.invokeMethod(Invoker.java:643)\n" +
        "\tat org.testng.internal.Invoker.invokeTestMethod(Invoker.java:820)\n" +
        "\tat org.testng.internal.Invoker.invokeTestMethods(Invoker.java:1128)\n" +
        "\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:129)\n" +
        "\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:112)\n" +
        "\tat org.testng.TestRunner.privateRun(TestRunner.java:782)\n" +
        "\tat org.testng.TestRunner.run(TestRunner.java:632)\n" +
        "\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:366)\n" +
        "\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:361)\n" +
        "\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:319)\n" +
        "\tat org.testng.SuiteRunner.run(SuiteRunner.java:268)\n" +
        "\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\n" +
        "\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\n" +
        "\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1244)\n" +
        "\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1169)\n" +
        "\tat org.testng.TestNG.run(TestNG.java:1064)\n" +
        "\tat org.testng.IDEARemoteTestNG.run(IDEARemoteTestNG.java:74)\n" +
        "\tat org.testng.RemoteTestNGStarter.main(RemoteTestNGStarter.java:121)\n" +
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n" +
        "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n" +
        "\tat java.lang.reflect.Method.invoke(Method.java:497)\n" +
        "\tat com.intellij.rt.execution.application.AppMain.main(AppMain.java:144)\n");
    scenario.addStep(step3);

    return scenario;
  }

  private String getExpectedHtml() throws IOException {
    String html = "";

    Reader reader = new InputStreamReader(getClass().getResourceAsStream("/SampleOutputHTMLPrinter.html"));
    BufferedReader bufferedReader = new BufferedReader(reader);
    String line = null;
    String lineSeparator = "";
    while ((line = bufferedReader.readLine()) != null) {
      html += lineSeparator + line;
      lineSeparator = "\n";
    }

    return html;
  }
}
