/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.parsers.xlsx;

import org.provatesting.expectations.DataType;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.expectations.InitParameter;
import org.provatesting.parsers.ScenarioParser;
import org.provatesting.scenario.Scenario;
import org.provatesting.scenario.Step;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.util.Iterator;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class XlsxParserTest {

  @Test
  public void baseScenario() {
    InputStream fileResource = XlsxParserTest.class.getResourceAsStream("/XlsxParserTestResource.xlsx");

    ScenarioParser xlsxParser = new XlsxParser();

    Scenario scenario = xlsxParser.parse("My Folder", "My Scenario", fileResource);

    assertNotNull(scenario);
    assertEquals("My Scenario", scenario.getName());
    assertEquals(1, scenario.getSteps().size());

    Step step = scenario.getSteps().iterator().next();
    assertEquals(step.getRowNumber(), 1);
    assertEquals(step.getResource(), "TestResource");
    assertEquals(step.getAction(), "MethodOne");

    assertEquals(step.getInitParams().size(), 1);
    InitParameter initParam = step.getInitParams().iterator().next();
    assertEquals(initParam.getName(), "InitParameter_1");
    assertEquals(initParam.getValue(), "InitParameter_1__Value");

    assertEquals(step.getExpectedRows().size(), 1);
    ExpectedRow row = step.getExpectedRows().iterator().next();

    assertEquals(row.getExpectedColumns().size(), 3);
    Iterator<ExpectedColumn> columns = row.getExpectedColumns().iterator();

    ExpectedColumn expectedColumn1 = columns.next();
    assertEquals(expectedColumn1.getColumnNumber(), 2);
    assertEquals(expectedColumn1.getKey(), "Key_one");
    assertEquals(expectedColumn1.getDataType(), DataType.Text);
    assertEquals(expectedColumn1.getPrecision(), "");
    assertEquals(expectedColumn1.getValue(), "Value_one");

    ExpectedColumn expectedColumn2 = columns.next();
    assertEquals(expectedColumn2.getColumnNumber(), 3);
    assertEquals(expectedColumn2.getKey(), "Key_two");
    assertEquals(expectedColumn2.getDataType(), DataType.Number);
    assertEquals(expectedColumn2.getPrecision(), "0.001");
    assertEquals(expectedColumn2.getValue(), "2.0");

    ExpectedColumn expectedColumn3 = columns.next();
    assertEquals(expectedColumn3.getColumnNumber(), 4);
    assertEquals(expectedColumn3.getKey(), "Key_three");
    assertEquals(expectedColumn3.getDataType(), DataType.Number);
    assertEquals(expectedColumn3.getPrecision(), "0.001");
    assertEquals(expectedColumn3.getValue(), "2.0");
  }

  @Test
  public void cellFormatIssue() {
    InputStream fileResource = XlsxParserTest.class.getResourceAsStream("/XlsxParserTestResource_IncorrectCellFormatErrorDuringParsing.xlsx");

    ScenarioParser xlsxParser = new XlsxParser();

    Scenario scenario = xlsxParser.parse("My Folder", "My Scenario", fileResource);
    Assert.assertNotNull(scenario);
  }

  @Test
  public void foo() {
    InputStream fileResource = XlsxParserTest.class.getResourceAsStream("/XlsxParserTestResource_CellsOutOfOrderInXMLFile.xlsx");

    ScenarioParser xlsxParser = new XlsxParser();

    Scenario scenario = xlsxParser.parse("My Folder", "My Scenario", fileResource);

    assertNotNull(scenario);
    assertEquals("My Scenario", scenario.getName());
    assertEquals(3, scenario.getSteps().size());

    Iterator<Step> steps = scenario.getSteps().iterator();

//Step 1
    Step step = steps.next();
    assertEquals(step.getRowNumber(), 1);
    assertEquals(step.getResource(), "AccountBogieDao");
    assertEquals(step.getAction(), "ValidateAccountBogies");

    assertEquals(step.getInitParams().size(), 0);

    assertEquals(step.getExpectedRows().size(), 0);

    assertEquals(step.getKeys().size(), 5);
    assertEquals(step.getKeys().get(0), "AccountNumber");
    assertEquals(step.getKeys().get(1), "BogieNumber");
    assertEquals(step.getKeys().get(2), "WhichBogie");
    assertEquals(step.getKeys().get(3), "StartDate");
    assertEquals(step.getKeys().get(4), "EndDate");

//Step 2
    step = steps.next();
    assertEquals(step.getRowNumber(), 7);
    assertEquals(step.getResource(), "AccountHelper");
    assertEquals(step.getAction(), "createAccount");

    assertEquals(step.getInitParams().size(), 0);

    assertEquals(step.getExpectedRows().size(), 1);

    ExpectedRow row = step.getExpectedRows().iterator().next();
    Iterator<ExpectedColumn> columns = row.getExpectedColumns().iterator();
    validateColumn("AccountNumber", DataType.Text, "", "9998.0", columns.next());
    validateColumn("AsOfDate", DataType.Text, "", "2012-10-04", columns.next());
    validateColumn("Bogie1", DataType.Text, "", "7277.0", columns.next());
    validateColumn("Bogie2", DataType.Text, "", "0.0", columns.next());

//Step 3
    step = steps.next();
    assertEquals(step.getRowNumber(), 14);
    assertEquals(step.getResource(), "AccountBogieDao");
    assertEquals(step.getAction(), "ValidateAccountBogies");

    assertEquals(step.getInitParams().size(), 0);

    assertEquals(step.getExpectedRows().size(), 2);

    Iterator<ExpectedRow> rows = step.getExpectedRows().iterator();
    row = rows.next();
    columns = row.getExpectedColumns().iterator();
    validateColumn("AccountNumber", DataType.Text, "", "9998.0", columns.next());
    validateColumn("BogieNumber", DataType.Text, "", "7277", columns.next());
    validateColumn("WhichBogie", DataType.Number, "0.0", "1.0", columns.next());
    validateColumn("StartDate", DataType.Text, "", "2012-10-04", columns.next());
    validateColumn("EndDate", DataType.Text, "", "2012-10-04", columns.next());

    row = rows.next();
    columns = row.getExpectedColumns().iterator();
    validateColumn("AccountNumber", DataType.Text, "", "9998.0", columns.next());
    validateColumn("BogieNumber", DataType.Text, "", "0", columns.next());
    validateColumn("WhichBogie", DataType.Number, "0.0", "2.0", columns.next());
    validateColumn("StartDate", DataType.Text, "", "2012-10-04", columns.next());
    validateColumn("EndDate", DataType.Text, "", "2012-10-04", columns.next());
  }

  private void validateColumn(String key, DataType dataType, String precision, String value, ExpectedColumn column) {
    assertEquals(column.getKey(), key);
    assertEquals(column.getDataType(), dataType);
    assertEquals(column.getPrecision(), precision);
    assertEquals(column.getValue(), value);
  }
}
