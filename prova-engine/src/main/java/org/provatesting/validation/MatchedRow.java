/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;

public class MatchedRow {
  private ExpectedRow expectedRow;
  private ActualRow actualRow;

  private List<MatchedColumn> matchedColumns;
  private List<ExpectedColumn> unmatchedExpectedColumns;
  private List<ActualColumn> unmatchedActualColumns;

  /**
   * Matched Row combines an expected and actual row to allow for validation.
   * @param expectedRow - the expected row
   * @param actualRow - the actual row
   */
  public MatchedRow(ExpectedRow expectedRow, ActualRow actualRow) {
    this.expectedRow = expectedRow;
    this.actualRow = actualRow;

    organize();
  }

  private void organize() {
    matchedColumns = new ArrayList<>();
    unmatchedExpectedColumns = new ArrayList<>();
    unmatchedActualColumns = new ArrayList<>();

    Map<String, ActualColumn> actualColumnsByKey = new HashMap<>();
    for (ActualColumn actualColumn : actualRow.getActualColumns()) {
      actualColumnsByKey.put(actualColumn.getKey(), actualColumn);
    }

    Set<String> matchedColumnNames = new HashSet<>();
    for (ExpectedColumn expectedColumn : expectedRow.getExpectedColumns()) {
      ActualColumn actualColumn = actualColumnsByKey.get(expectedColumn.getKey());
      if (actualColumn != null) {
        matchedColumns.add(new MatchedColumn(expectedColumn, actualColumn));
        matchedColumnNames.add(expectedColumn.getKey());
      } else if (expectedColumn.getKey().startsWith("|input|")) {
        ActualColumn fakeActual = new ActualColumn(expectedColumn.getKey(), expectedColumn.getValue());
        matchedColumns.add(new MatchedColumn(expectedColumn, fakeActual));
        matchedColumnNames.add(expectedColumn.getKey().substring(7));
      } else {
        unmatchedExpectedColumns.add(expectedColumn);
      }
    }

    for (ActualColumn actualColumn : actualColumnsByKey.values()) {
      if (!matchedColumnNames.contains(actualColumn.getKey())) {
        unmatchedActualColumns.add(actualColumn);
      }
    }
  }

  /**
   * Calculates the match rate of the two rows. The higher the match rate, the better.
   * The actual algorithm is straight forward:
   *   - we create a string of 1s and 0s
   *   - if a column matches, we add 1 to the string.
   *   - if a column does not match, we add 0 to the string.
   *   - when all columns were evaluated, we generate a double out of that string
   *   - we then divide by the 10^num_columns
   *   - this value is the second "half" of the match rate
   *   - the first "half" is just a count of the number of columns that matched
   *
   * @return the actual match rate
   */
  public double getMatchRate() {
    long matched = 0;
    StringBuilder pattern = new StringBuilder();

    for (MatchedColumn matchedColumn : getMatchedColumns()) {
      if (matchedColumn.valueIsMatch()) {
        matched++;
        pattern.append("1");
      } else {
        pattern.append("0");
      }
    }

    double patternAsDouble = 0d;
    if (pattern.length() > 0) {
      int count = matchedColumns.size() + unmatchedExpectedColumns.size();
      patternAsDouble = Double.parseDouble(pattern.toString()) / Math.pow(10d, (double) count);
    }

    return (double) matched + patternAsDouble;
  }

  /**
   * Calculates the percentage of the match rate.
   * A higher percentage means a better match.
   * @return the percentage of the match
   */
  public double getMatchPercent() {
    double matchRate = getMatchRate();
    double possibleScore = getPossibleScore();
    return matchRate / possibleScore;
  }

  private double getPossibleScore() {
    int count = matchedColumns.size() + unmatchedExpectedColumns.size();
    StringBuilder pattern = new StringBuilder();
    for (int i = 0; i < count; i++) {
      pattern.append("1");
    }
    double patternAsDouble = 0d;
    if (pattern.length() > 0) {
      patternAsDouble = Double.parseDouble(pattern.toString()) / Math.pow(10d, (double) count);
    }
    return (double) count + patternAsDouble;
  }

  public ActualRow getActualRow() {
    return actualRow;
  }

  public ExpectedRow getExpectedRow() {
    return expectedRow;
  }

  public List<MatchedColumn> getMatchedColumns() {
    return matchedColumns;
  }

  public List<ExpectedColumn> getUnmatchedExpectedColumns() {
    return unmatchedExpectedColumns;
  }

  public List<ActualColumn> getUnmatchedActualColumns() {
    return unmatchedActualColumns;
  }
}
