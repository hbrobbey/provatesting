/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.validation;

import java.math.BigDecimal;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.expectations.DataType;
import org.provatesting.expectations.ExpectedColumn;

//TODO: clean up to have the matched column be an interface / base class,
//      whose subclasses are instantiated in the DataType enum?
public class MatchedColumn {
  private ExpectedColumn expectedColumn;
  private ActualColumn actualColumn;

  /**
   * Matched Column represents an expected and actual value pair.
   * @param expectedColumn - the expected value from the scenario
   * @param actualColumn - the actual value from the execution of the action
   */
  public MatchedColumn(ExpectedColumn expectedColumn, ActualColumn actualColumn) {
    this.expectedColumn = expectedColumn;
    this.actualColumn = actualColumn;
  }

  public String getKey() {
    return expectedColumn.getKey();
  }

  /**
   * Indicates if the expected and actual values match according to the precision.
   * @return true if, and only if, the values match according to the specs
   */
  public boolean valueIsMatch() {
    Object expected;
    Object actual;

    if (DataType.Number == getDataType()) {
      int precision = 2;
      try {
        if (expectedColumn.getPrecision().contains(".")) {
          precision = expectedColumn.getPrecision().length()
              - expectedColumn.getPrecision().indexOf(".") - 1;
        }
        precision = Integer.parseInt(expectedColumn.getPrecision());
      } catch (NumberFormatException e) {
        //Move on w/o complaining...we are going to use default precisions
      }

      expected = getBigDecimalValue(getExpectedValue(), precision);
      actual = getBigDecimalValue(getActualValue(), precision);
    } else {
      expected = getExpectedValue();
      actual = getActualValue();
    }

    return expectedAndActualAreEqual(expected, actual);
  }

  private BigDecimal getBigDecimalValue(String value, int precision) {
    if (value == null) {
      return null;
    }

    try {
      return new BigDecimal(value).setScale(precision, BigDecimal.ROUND_HALF_EVEN);
    } catch (Throwable t) {
      return null;
    }
  }

  private boolean expectedAndActualAreEqual(Object expected, Object actual) {
    if (expected == null) {
      return actual == null;
    } else {
      if ("@{ANY}@".equals(expected)) {
        return true;
      } else if ("@{NULL}@".equals(expected)) {
        return actual == null || "".equals(actual);
      } else if ("@{NOT_NULL}@".equals(expected)) {
        return !(actual == null || "".equals(actual));
      }
      return expected.equals(actual);
    }
  }

  private DataType getDataType() {
    return expectedColumn != null ? expectedColumn.getDataType() : DataType.Text;
  }

  public String getExpectedValue() {
    return expectedColumn != null ? expectedColumn.getValue() : "";
  }

  public String getActualValue() {
    return actualColumn != null ? actualColumn.getValue() : "";
  }
}
