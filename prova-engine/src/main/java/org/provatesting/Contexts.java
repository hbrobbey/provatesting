/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting;

import com.google.common.reflect.ClassPath;
import org.provatesting.utils.ClassPathAnalyzer;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Contexts {

  private static final Logger LOG = LoggerFactory.getLogger(Contexts.class);

  private final List<Context> instances;

  /**
   * Default constructor to initialize all known contexts using reflection.
   */
  public Contexts() {
    ClassPathAnalyzer classPathAnalyzer = new ClassPathAnalyzer();
    Set<Class<? extends Context>> contextClasses = classPathAnalyzer.getImplementationsOf(Context.class);
    instances = instantiateContextClasses(contextClasses);
  }

  private List<Context> instantiateContextClasses(Set<Class<? extends Context>> contextClasses) {
    List<Context> contextInstances = new ArrayList<>();
    for (Class<? extends Context> contextClass : contextClasses) {
      try {
        contextInstances.add(contextClass.newInstance());
      } catch (InstantiationException | IllegalAccessException e) {
        LOG.error("Found context class, but could not instantiate it.", e);
      }
    }
    LOG.info("Found {} contexts on classpath.", contextInstances.size());
    return contextInstances;
  }

  /**
   * Provides access to a given resource.
   * @param resourceName - the name of the resource
   * @return - returns null if no resource of the given name is found, a new instance otherwise
   * @throws RuntimeException if the resource cannot be instantiated
   */
  public Object get(String resourceName) {
    for (Context context : instances) {
      Class resource = context.get(resourceName);
      if (resource != null) {
        try {
          return resource.newInstance();
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    }
    return null;
  }

  public static class ResourceNotFound extends RuntimeException {
  }
}
