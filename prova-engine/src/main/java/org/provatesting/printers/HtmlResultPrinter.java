/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.printers;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.provatesting.actuals.ActualColumn;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.expectations.InitParameter;
import org.provatesting.scenario.Scenario;
import org.provatesting.scenario.Step;
import org.provatesting.validation.MatchedColumn;
import org.provatesting.validation.MatchedRow;

public class HtmlResultPrinter implements ResultPrinter {

  private Writer writer;

  public HtmlResultPrinter(Writer writer) {
    this.writer = writer;
  }

  @Override
  public void print(Scenario scenario) {
    String html = generateHtml(scenario);
    writeHtml(html);
  }

  private void writeHtml(String html) {
    try {
      writer.write(html);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        writer.flush();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private String generateHtml(Scenario scenario) {
    StringBuilder html = new StringBuilder(
        "<html>"
            + "<head>"
            + "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
            + "<link rel=\"stylesheet\" type=\"text/css\" href=\"provatesting.css\">"
            + ""
            + "<script type=\"text/javascript\" "
            + "src=\"http://code.jquery.com/jquery-1.7.1.min.js\"></script>"
            
            + "<script type=\"text/javascript\" "
            + "src=\"provatesting.js\"></script>"
            
            + "</head>"
            + ""
            + "<body>"
            + "<div class=\"row\">"
            
            + "<h2><div class=\"cell\">Scenario:</div><div class=\"cell\">"
            + scenario.getName()
            + "</div></h2>"
            
            + "</div>"
            + "<div class=\"row\">"
            + "<h3><div class=\"cell\">Status:</div><div class=\"cell status "
            + (scenario.isValid() ? "passed" : "failed")
            + "\">"
            + (scenario.isValid() ? "PASSED" : "FAILED")
            + "</div></h3>"
            + "</div>"
            + ""
            + "<p><span height=\"30px\">&nbsp;</span></p>"
    );

    String spacer = "";
    for (Step step : scenario.getSteps()) {
      if (step.isException()) {
        html.append(spacer + parseExceptionStep(step));
      } else {
        html.append(spacer + parseNormalStep(step));
      }
      spacer = "<p/>";
    }

    html.append(
        "</body>"
            + "</html>"
    );

    return html.toString();
  }

  private String parseExceptionStep(Step step) {
    return ""
        + "<div class=\"table\">"
        + "<div class=\"row\">"
        
        + "<span class=\"header error\">"
        + step.getResource() + "." + step.getAction()
        + "</span>"
        
        + "</div>"
        + "<div class=\"row error\">"
        + "<div class=\"table error\">"
        + "<div class=\"row\">"
        + "<span class=\"cell\">Start Row</span>"
        + "<span class=\"cell\">" + step.getRowNumber() + "</span>"
        + "</div>"
        + "<div class=\"row\">"
        + "<span class=\"cell\"></span>"
        + "<span class=\"cell error\">" + step.getExceptionMessage() + "</span>"
        + "</div>"
        + "</div>"
        + "</div>"
        + "</div>";
  }

  private String parseNormalStep(Step step) {
    String status = step.isValid() ? "passed" : "failed";

    //add step header
    StringBuilder partialHtml = new StringBuilder(
        "<div class=\"table\">"
            + "<div class=\"row\">"

            + "<span class=\"header " + status + "\">"
            + step.getResource()
            + "."
            + step.getAction()
            + "</span>"

            + "</div>"
            + "<div class=\"row " + status + "\">"
            + "<div class=\"table " + status + "\">"
            + "<div class=\"row\">"
            + "<span class=\"cell\">Start Row</span>"
            + "<span class=\"cell\">" + step.getRowNumber() + "</span>"
            + "</div>"
    );

    //add init params
    for (InitParameter initParam : step.getInitParams()) {
      partialHtml.append(""
          + "<div class=\"row\">"
          + "<span class=\"cell\">Init Parameter</span>"
          + "<span class=\"cell\">" + initParam.getName() + "</span>"
          + "<span class=\"cell\">" + initParam.getValue() + "</span>"
          + "</div>"
      );
    }

    //add keys
    partialHtml.append(
        "<div class=\"row\">"
            + "<span class=\"cell\">Keys</span>"
    );
    for (String key : step.getKeys()) {
      partialHtml.append("<span class=\"cell\">" + key + "</span>");
    }
    partialHtml.append("</div>");

    //add matched rows
    for (MatchedRow row : step.getMatchedRows()) {
      partialHtml.append(
          "<div class=\"row\">"
              + "<span class=\"cell\">Values</span>"
      );

      for (MatchedColumn column : row.getMatchedColumns()) {
        if (column.valueIsMatch()) {
          partialHtml.append(
              "<span class=\"cell passed\">"
              + column.getExpectedValue()
              + "</span>"
          );
        } else {
          partialHtml.append(
              "<div class=\"cell failed\">"
                  + "<div class=\"value\">" + column.getExpectedValue() + "</div>"
                  + "<div class=\"label\">Expected</div>"
                  + "<div class=\"value\">" + column.getActualValue() + "</div>"
                  + "<div class=\"label\">Actual</div>"
                  + "</div>"
          );
        }
      }

      partialHtml.append("</div>");
    }

    //missing expected
    for (ActualRow row : step.getUnmatchedActualRows()) {
      partialHtml.append(
          "<div class=\"row\">"
              + "<span class=\"cell\">Missing Expected</span>"
      );

      Map<String, String> actualsByKey = new HashMap<>();
      for (ActualColumn column : row.getActualColumns()) {
        actualsByKey.put(column.getKey(), column.getValue());
      }
      for (String key : step.getKeys()) {
        partialHtml.append("<span class=\"cell missing\">" + actualsByKey.get(key) + "</span>");
      }

      partialHtml.append("</div>");
    }

    //missing actual
    for (ExpectedRow row : step.getUnmatchedExpectedRows()) {
      partialHtml.append(
          "<div class=\"row\">"
              + "<span class=\"cell\">Missing Actual</span>"
      );

      for (ExpectedColumn column : row.getExpectedColumns()) {
        partialHtml.append("<span class=\"cell missing\">" + column.getValue() + "</span>");
      }

      partialHtml.append("</div>");
    }

    //wrapping up the partial
    partialHtml.append(
        "</div>"
            + "</div>"
            + "</div>"
    );

    return partialHtml.toString();
  }

  @Override
  public void close() throws IOException {
    writer.close();
  }
}
