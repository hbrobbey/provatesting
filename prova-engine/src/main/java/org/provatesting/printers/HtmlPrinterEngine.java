/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.printers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class HtmlPrinterEngine implements PrinterEngine {

  private final List<SummaryInfo> summaries = new ArrayList<>();

  @Override
  public String getName() {
    return "HTML";
  }

  @Override
  public ResultPrinter prepareForScenario(String folderName, String fileName, boolean success) {
    String fullPath = folderName + "/" + fileName + ".html";
    summaries.add(new SummaryInfo(folderName, fileName, success));

    exportResource(folderName, "/provatesting.css");
    exportResource(folderName, "/provatesting.js");

    try {
      Writer writer = new OutputStreamWriter(
          new FileOutputStream(fullPath),
          Charset.defaultCharset()
      );
      return new HtmlResultPrinter(writer);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void finalizePrinting(String folderName) {
    exportResource(folderName, "/provatesting.css");
    exportResource(folderName, "/provatesting.js");

    try (Writer writer = new OutputStreamWriter(
            new FileOutputStream(folderName + "/Summary.html"),
            Charset.defaultCharset())
    ) {
      printSummary(writer);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void exportResource(String folderName, String resourceName) {
    InputStream in = null;
    OutputStream out = null;

    try {
      in = this.getClass().getResourceAsStream(resourceName);
      if (in == null) {
        throw new RuntimeException("Resource not found: " + resourceName);
      }

      int readBytes;
      byte[] buffer = new byte[4096];
      out = new FileOutputStream(folderName + resourceName);
      while ((readBytes = in.read(buffer)) > 0) {
        out.write(buffer, 0, readBytes);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (Throwable t) {
          //nothing else to do...
        }
      }
      if (out != null) {
        try {
          out.close();
        } catch (Throwable t) {
          //nothing else to do...
        }
      }
    }
  }

  private void printSummary(Writer writer) {
    String html = generateHtml();
    writeHtml(writer, html);
  }

  private String generateHtml() {
    boolean allValid = summaries.stream().allMatch(s -> s.success);

    StringBuilder html = new StringBuilder(
            "<html>"
                    + "<head>"
                    + "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
                    + "<link rel=\"stylesheet\" type=\"text/css\" href=\"provatesting.css\">"
                    + ""
                    + "<script type=\"text/javascript\" "
                    + "src=\"http://code.jquery.com/jquery-1.7.1.min.js\"></script>"

                    + "<script type=\"text/javascript\" "
                    + "src=\"provatesting.js\"></script>"

                    + "</head>"
                    + ""
                    + "<body>"
                    + "<div class=\"row\">"

                    + "<h2><div class=\"cell\">Summary:</div><div class=\"cell\">"
                    + "All_Scenarios"
                    + "</div></h2>"

                    + "</div>"
                    + "<div class=\"row\">"
                    + "<h3><div class=\"cell\">Status:</div><div class=\"cell status "
                    + (allValid ? "passed" : "failed")
                    + "\">"
                    + (allValid ? "PASSED" : "FAILED")
                    + "</div></h3>"
                    + "</div>"
                    + ""
                    + "<p><span height=\"30px\">&nbsp;</span></p>"
    );

    String spacer = "";
    for (SummaryInfo summary : summaries) {
      html.append(spacer);

      String status = summary.success ? "passed" : "failed";

      html.append(
              "<div class=\"table\">"
                      + "<div class=\"row\">"

                      + "<span class=\"header " + status + "\">"
                      + "<a class=\"header " + status + "\" href=\"" + summary.fullPath + "\">"
                      + summary.scenarioName
                      + "</a>"
                      + "</span>"

                      + "</div>"
                      + "</div>"

      );
      spacer = "<p/>";
    }

    html.append(
            "</body>"
                    + "</html>"
    );

    return html.toString();
  }

  private void writeHtml(Writer writer, String html) {
    try {
      writer.write(html);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        writer.flush();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private static class SummaryInfo {
      private String scenarioName;
      private String fullPath;
      private boolean success;

      SummaryInfo(String path, String scenarioName, boolean success) {
          this.scenarioName = scenarioName;
          this.fullPath = path + "/" + scenarioName + ".html";
          this.success = success;
      }
    }
}
