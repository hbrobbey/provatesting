/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.scenario;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.poi.ss.usermodel.Cell;
import org.provatesting.actuals.ActualRow;
import org.provatesting.expectations.ExpectedColumn;
import org.provatesting.expectations.ExpectedRow;
import org.provatesting.expectations.InitParameter;
import org.provatesting.parsers.xlsx.XlsxParser;
import org.provatesting.validation.MatchedRow;

public class Step implements Comparable<Step> {

  private int rowNumber;
  private String resource;
  private String action;

  private SortedSet<InitParameter> initParams;
  private List<String> keys;
  private SortedSet<ExpectedRow> expectedRows;

  private Set<ActualRow> actualRows;
  private String exceptionMessage;
  private Collection<MatchedRow> matchedRows;
  private SortedSet<ExpectedRow> unmatchedExpectedRows;
  private Set<ActualRow> unmatchedActualRows;

  /**
   * Represents expected and actual values combined in one object.
   * @param rowNumber - the position of the row in the table
   * @param resourceName - the resource that this step corresponds to
   * @param actionName - the action that this step corresponds to
   */
  public Step(int rowNumber, String resourceName, String actionName) {
    this.rowNumber = rowNumber;
    this.resource = resourceName;
    this.action = actionName;

    this.initParams = new TreeSet<>();
    this.keys = new ArrayList<>();
    this.expectedRows = new TreeSet<>();

    this.actualRows = new HashSet<>();
  }

  public int getRowNumber() {
    return rowNumber;
  }

  public String getResource() {
    return resource;
  }

  public String getAction() {
    return action;
  }

  public SortedSet<InitParameter> getInitParams() {
    return initParams;
  }

  public SortedSet<ExpectedRow> getExpectedRows() {
    return expectedRows;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public boolean isException() {
    return exceptionMessage != null;
  }

  public void addInitParam(InitParameter initParameter) {
    initParams.add(initParameter);
  }

  /**
   * Adds an expected row to the step.
   * @param expectedRow - the row
   */
  public void addExpectedRow(ExpectedRow expectedRow) {
    if (keys.size() == 0) {
      for (ExpectedColumn column : expectedRow.getExpectedColumns()) {
        keys.add(column.getKey());
      }
    }
    expectedRows.add(expectedRow);
  }

  public void addActualRow(ActualRow actualRow) {
    actualRows.add(actualRow);
  }

  public boolean isValid() {
    return validate();
  }

  public Collection<MatchedRow> getMatchedRows() {
    return matchedRows;
  }

  public SortedSet<ExpectedRow> getUnmatchedExpectedRows() {
    return unmatchedExpectedRows;
  }

  public Set<ActualRow> getUnmatchedActualRows() {
    return unmatchedActualRows;
  }

  private boolean validate() {
    matchedRows = new ArrayList<>();
    unmatchedActualRows = new HashSet<>(actualRows);
    unmatchedExpectedRows = new TreeSet<>(expectedRows);

    if (unmatchedExpectedRows.isEmpty() && actualRows.isEmpty()) {
      return true;
    }

    //First, perfect matches
    Iterator<ExpectedRow> expectedRowsThatNeedMatching = unmatchedExpectedRows.iterator();
    while (expectedRowsThatNeedMatching.hasNext()) {
      ExpectedRow expectedRow = expectedRowsThatNeedMatching.next();
      for (ActualRow actualRow : actualRows) {
        MatchedRow candidate = new MatchedRow(expectedRow, actualRow);
        if (candidate.getMatchPercent() == 1d) {
          matchedRows.add(candidate);
          unmatchedActualRows.remove(candidate.getActualRow());

          expectedRowsThatNeedMatching.remove();
          break;
        }
      }
    }

    //Find best potential matches for the ones that are not a complete match
    Map<Integer, Map<ActualRow, MatchedRow>> actualRowRankingsByExpectedRow = new HashMap<>();
    Map<ActualRow, Map<Integer, MatchedRow>> expectedRowRankingsByActualRow = new HashMap<>();

    for (ExpectedRow expectedRow : unmatchedExpectedRows) {
      for (ActualRow actualRow : unmatchedActualRows) {
        MatchedRow candidate = new MatchedRow(expectedRow, actualRow);
        if (candidate.getMatchRate() > 0d) {
          actualRowRankingsByExpectedRow
              .computeIfAbsent(expectedRow.getRowNumber(), HashMap::new)
              .put(actualRow, candidate);

          expectedRowRankingsByActualRow
              .computeIfAbsent(actualRow, k -> new HashMap<>())
              .put(expectedRow.getRowNumber(), candidate);
        }
      }
    }

    expectedRowsThatNeedMatching = unmatchedExpectedRows.iterator();
    while (expectedRowsThatNeedMatching.hasNext()) {
      ExpectedRow expectedRow = expectedRowsThatNeedMatching.next();

      MatchedRow bestMatch = findBestMatch(
          expectedRow,
          actualRowRankingsByExpectedRow,
          expectedRowRankingsByActualRow
      );

      if (bestMatch != null) {
        matchedRows.add(bestMatch);
        unmatchedActualRows.remove(bestMatch.getActualRow());

        expectedRowsThatNeedMatching.remove();
      }
    }

    double matchingAverage = 0d;
    if (!matchedRows.isEmpty()) {
      matchingAverage = matchedRows.parallelStream()
          .mapToDouble(MatchedRow::getMatchPercent)
          .average()
          .getAsDouble();
    }

    return matchedRows.size() == expectedRows.size() && matchingAverage == 1d;
  }

  private MatchedRow findBestMatch(ExpectedRow expectedRow,
                                   Map<Integer, Map<ActualRow, MatchedRow>> actualRowRankings,
                                   Map<ActualRow, Map<Integer, MatchedRow>> expectedRowRankings) {

    if (actualRowRankings.containsKey(expectedRow.getRowNumber())) {
      return findBestMatch(actualRowRankings.get(
          expectedRow.getRowNumber()).values(),
          expectedRowRankings,
          new HashSet<>()
      );
    } else {
      return null;
    }
  }

  private MatchedRow findBestMatch(Collection<MatchedRow> candidates,
                                   Map<ActualRow, Map<Integer, MatchedRow>> expectedRowRankings,
                                   Set<MatchedRow> alreadyVisited) {

    MatchedRow highestRankingByExpectedRow = findHighestRankingElement(candidates, alreadyVisited);
    if (highestRankingByExpectedRow == null) {
      return null;
    }

    //check that no other expected row has a better match
    Map<Integer, MatchedRow> expectedRowRankingsForActualRow = expectedRowRankings.get(
        highestRankingByExpectedRow.getActualRow()
    );

    if (expectedRowRankingsForActualRow.size() == 1) {
      return highestRankingByExpectedRow;
    }

    MatchedRow crossCheckWithActualRowRanking = findHighestRankingElement(
        expectedRowRankingsForActualRow.values(),
        new HashSet<>()
    );

    if (highestRankingByExpectedRow == crossCheckWithActualRowRanking) {
      return highestRankingByExpectedRow;
    } else {
      alreadyVisited.add(highestRankingByExpectedRow);
      return findBestMatch(candidates, expectedRowRankings, alreadyVisited);
    }
  }

  private MatchedRow findHighestRankingElement(Collection<MatchedRow> candidates,
                                               Set<MatchedRow> alreadyVisited
  ) {

    MatchedRow bestMatch = null;
    for (MatchedRow candidate : candidates) {
      if (alreadyVisited.contains(candidate)) {
        continue;
      }

      if (bestMatch == null) {
        bestMatch = candidate;
      } else if (candidate.getMatchRate() > bestMatch.getMatchRate()) {
        bestMatch = candidate;
      }
    }
    return bestMatch;
  }

  public List<String> getKeys() {
    return keys;
  }

  /**
   * Add multiple keys at once.
   * @param keys - the keys
   */
  public void addKeys(XlsxParser.CellsInOrder keys) {
    Iterator<Cell> keysIter = keys.iterator();

    while (keysIter.hasNext()) {
      Cell key = keysIter.next();

      if (key.getStringCellValue() == null || "".equals(key.getStringCellValue().trim())) {
        break;  //really ugly....
      }

      if ("Keys".equals(key.getStringCellValue())) {
        continue;
      }

      this.keys.add(key.getStringCellValue());
    }
  }

  @Override
  public int compareTo(Step o) {
    return Integer.compare(rowNumber, o.getRowNumber());
  }

  /**
   * Compares an instance of this type with a given object.
   * @param o - the object to compare to
   * @return true, only if the two objects are of the same type and all properties are equal
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Step step = (Step) o;
    return rowNumber == step.rowNumber
        && Objects.equals(resource, step.resource)
        && Objects.equals(action, step.action)
        && Objects.equals(initParams, step.initParams)
        && Objects.equals(keys, step.keys)
        && Objects.equals(expectedRows, step.expectedRows)
        && Objects.equals(actualRows, step.actualRows)
        && Objects.equals(exceptionMessage, step.exceptionMessage)
        && Objects.equals(matchedRows, step.matchedRows)
        && Objects.equals(unmatchedExpectedRows, step.unmatchedExpectedRows)
        && Objects.equals(unmatchedActualRows, step.unmatchedActualRows);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        rowNumber,
        resource,
        action,
        initParams,
        keys,
        expectedRows,
        actualRows,
        exceptionMessage,
        matchedRows,
        unmatchedExpectedRows,
        unmatchedActualRows
    );
  }
}
