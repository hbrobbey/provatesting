/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.utils;

import com.google.common.reflect.ClassPath;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

public class ClassPathAnalyzer {

    private static final Logger LOG = LoggerFactory.getLogger(ClassPathAnalyzer.class);

    public <T> Set<Class<? extends T>> getImplementationsOf(Class<T> toLookFor) {
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        if (cl instanceof URLClassLoader) {
            URL[] urls = ((URLClassLoader) cl).getURLs();
            LOG.trace("The following libraries were loaded...");
            for (URL url : urls) {
                LOG.trace("\t{}", url);
            }
        } else {
            LOG.trace("ClassLoader is not of type URLClassLoader - therefore cannot list all classes");
        }

        Set<Class<? extends T>> contextClasses = new HashSet<>();
        contextClasses.addAll(findContextClassesUsingGuava(toLookFor));
        contextClasses.addAll(findContextClassesUsingReflections(toLookFor));

        return contextClasses;
    }

    private <T> Set<Class<? extends T>> findContextClassesUsingGuava(Class<T> toLookFor) {
        Set<Class<? extends T>> contextClasses = new HashSet<>();
        ClassPath cp = null;
        try {
            cp = ClassPath.from(Thread.currentThread().getContextClassLoader());
        } catch (IOException e) {
            LOG.trace("Could not get full classpath.", e);
        }

        if (cp != null) {
            for (ClassPath.ClassInfo classInfo : cp.getTopLevelClasses()) {
                try {
                    Class clazz = classInfo.load();
                    if (!toLookFor.equals(clazz) && toLookFor.isAssignableFrom(clazz)) {
                        contextClasses.add(clazz);
                    }
                } catch (Throwable t) {
                    LOG.trace("Could not load class {}", classInfo.getName(), t);
                }
            }
        }
        return contextClasses;
    }

    private <T> Set<Class<? extends T>> findContextClassesUsingReflections(Class<T> toLookFor) {
        Reflections reflections = new Reflections("");
        return reflections.getSubTypesOf(toLookFor);
    }

}
