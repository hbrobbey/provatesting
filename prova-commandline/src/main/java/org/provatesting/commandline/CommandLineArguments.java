/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.commandline;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommandLineArguments {

  private static final Options options = new Options();

  static {
    Option parser = Option.builder("s")
        .argName("scenarioParser")
        .desc("The scenario parser (XLSX).")
        .hasArg()
        .required()
        .build();

    Option printer = Option.builder("r")
        .argName("resultPrinter")
        .desc("The result printer (HTML).")
        .hasArg()
        .required()
        .build();

    Option test = Option.builder("t")
        .argName("testResource")
        .desc("The absolute path to the test resource; can be a folder or a single file.")
        .hasArg()
        .required()
        .build();

    Option result = Option.builder("o")
        .argName("resultOutputPath")
        .desc("The absolute path to the folder that will hold the results.")
        .hasArg()
        .build();

    options.addOption(parser);
    options.addOption(printer);
    options.addOption(test);
    options.addOption(result);
  }

  private String scenarioParser;
  private String resultPrinter;
  private String testResource;
  private String resultOutputPath;

  private boolean allGood;

  /**
   * Constructor to create the actual command line arguments.
   * @param args - the array of strings that will be parsed into meaningful arguments
   */
  public CommandLineArguments(String[] args) {
    CommandLineParser parser = new DefaultParser();
    try {
      CommandLine line = parser.parse(options, args);
      scenarioParser = line.getOptionValue("s");
      resultPrinter = line.getOptionValue("r");
      testResource = line.getOptionValue("t");
      resultOutputPath = line.getOptionValue("o");
      allGood = true;
    } catch (ParseException e) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("CommandLineRunner", options);
      allGood = false;
    }
  }

  public boolean isAllGood() {
    return allGood;
  }

  public String getScenarioParser() {
    return scenarioParser;
  }

  public String getResultPrinter() {
    return resultPrinter;
  }

  public String getTestResource() {
    return testResource;
  }

  public String getResultOutputPath() {
    return resultOutputPath;
  }
}
