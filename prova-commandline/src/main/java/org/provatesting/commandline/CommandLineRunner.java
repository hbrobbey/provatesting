/*
 Copyright 2020 Stefan Maucher

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.provatesting.commandline;

import org.provatesting.ProvaRunner;
import org.provatesting.parsers.ScenarioParser;
import org.provatesting.printers.PrinterEngine;
import org.provatesting.utils.ClassPathAnalyzer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class CommandLineRunner {

  /**
   * The main method that is used to get the whole thing going.
   * @param args - command line arguments that are used to determine certain aspects
   */
  public static void main(String[] args) {
    CommandLineArguments cla = new CommandLineArguments(args);
    if (!cla.isAllGood()) {
      System.exit(-1);
    }

    ScenarioParser parser = getScenarioParser(cla);
    PrinterEngine printer = getResultPrinter(cla);
    String outputPath = cla.getResultOutputPath();
    ProvaRunner provaRunner = new ProvaRunner(parser, printer, outputPath);

    List<File> testResources = getTestResources(cla);

    provaRunner.run(testResources);
  }

  private static ScenarioParser getScenarioParser(CommandLineArguments cla) {
    String scenarioParserName = cla.getScenarioParser();

    ClassPathAnalyzer classPathAnalyzer = new ClassPathAnalyzer();
    Set<Class<? extends ScenarioParser>> parsers = classPathAnalyzer.getImplementationsOf(ScenarioParser.class);
    for (Class<? extends ScenarioParser> parserClass : parsers) {
      try {
        ScenarioParser parser = parserClass.newInstance();
        if (parser.getName().equals(scenarioParserName)) {
          return parser;
        }
      } catch (Throwable t) {
        throw new RuntimeException(t);
      }
    }

    throw new RuntimeException("Parser " + scenarioParserName + " not supported.");
  }

  private static PrinterEngine getResultPrinter(CommandLineArguments cla) {
    String resultPrinterName = cla.getResultPrinter();

    ClassPathAnalyzer classPathAnalyzer = new ClassPathAnalyzer();
    Set<Class<? extends PrinterEngine>> printers = classPathAnalyzer.getImplementationsOf(PrinterEngine.class);

    for (Class<? extends PrinterEngine> printerEngineClazz : printers) {
      try {
        PrinterEngine printerEngine = printerEngineClazz.newInstance();
        if (printerEngine.getName().equalsIgnoreCase(resultPrinterName)) {
          return printerEngine;
        }
      } catch (Throwable t) {
        throw new RuntimeException(t);
      }
    }

    throw new RuntimeException("ResultPrinter " + resultPrinterName + " not supported.");
  }

  private static List<File> getTestResources(CommandLineArguments cla) {
    String fileName = cla.getTestResource();
    File file = new File(fileName);

    return getTestResources(file);
  }

  private static List<File> getTestResources(File file) {
    if (file == null) {
      return Collections.emptyList();
    }

    List<File> files = new ArrayList<>();
    if (file.isDirectory()) {
      File[] listFiles = file.listFiles();
      if (listFiles != null) {
        for (File sub : listFiles) {
          files.addAll(getTestResources(sub));
        }
      }
    } else {
      files.add(file);
    }
    return files;
  }
}
